package datacore

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

var tableTest = "product_test"

func init()  {
	// drop table if exist
	// each run test create table
	db := dbConn()
	defer db.Close()
	sqlDropTable := `DROP TABLE IF EXISTS ` + tableTest
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("Begin transaction failed:", err.Error())
		log.Fatal(err)
	}
	_, err = tx.ExecContext(ctx, sqlDropTable)
	if err != nil {
		log.Error("Drop table failed:", err.Error())
		tx.Rollback()
		return
	}
}

func TestProcess_new(t *testing.T) {
	log.Debugf("START TEST FUNCTION NEW ...")
	var id int

	// call function
	argTets := []string{"", "E:\\Golang\\datacore\\product_new_test.csv", "INT",
		"VARCHAR*255", "VARCHAR*255", "TIMESTAMP", "INT"}
	Process_new(tableTest, argTets)

	// get row db
	db := dbConn()
	defer db.Close()
	sqlSelect := `SELECT id FROM product_test WHERE id=?;`
	productRow := db.QueryRow(sqlSelect, 1)
	productRow.Scan(&id)

	assert.Equal(t, 1, id)
	log.Debugf("TEST DONE FUNCTION NEW ...")
}

func TestProcess_edit(t *testing.T) {
	log.Debugf("START TEST FUNCTION EDIT ...")
	var description string

	// call function
	pathFile := "E:\\Golang\\datacore\\product_edit_test.csv"
	Process_edit(tableTest, pathFile)

	// get row db
	db := dbConn()
	defer db.Close()
	sqlSelect := `SELECT description FROM product_test WHERE id=?;`
	productRow := db.QueryRow(sqlSelect, 1)
	productRow.Scan(&description)

	assert.Equal(t, "updated", description)
	log.Debugf("TEST DONE FUNCTION EDIT ...")
}

func TestProcess_del(t *testing.T) {
	log.Debugf("START TEST FUNCTION DELETE ...")
	var id int

	// call function
	pathFile := "E:\\Golang\\datacore\\product_del_test.csv"
	Process_del(tableTest, pathFile)

	// get row db
	db := dbConn()
	defer db.Close()
	sqlSelect := `SELECT id FROM product_test WHERE id=?;`
	productRow := db.QueryRow(sqlSelect, 1)
	productRow.Scan(&id)

	assert.Equal(t, 0, id)
	log.Debugf("TEST DONE FUNCTION DELETE ...")
}