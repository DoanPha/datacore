# Project Name

datacore

### Dependencies
* go
* testing
* github.com/stretchr/testify/assert

## Executing unit test

run test:
```
go test
```
