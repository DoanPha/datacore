package datacore

import (
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	s "strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/op/go-logging"
	"gopkg.in/yaml.v2"
)

var log = logging.MustGetLogger("datacore")

type DbConnect struct {
	Database struct {
		Type     string `yml:"type"`
		Host     string `yml:"host"`
		Port     string `yml:"port"`
		Name     string `yml:"name"`
		User     string `yml:"user"`
		Password string `yml:"password"`
		Sslmode  string `yml:"sslmode"`
	}
}

func (c *DbConnect) getConfigDB() *DbConnect {

	yamlFile, err := ioutil.ReadFile("datacore.yml")
	if err != nil {
		log.Error("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Error("Unmarshal: %v", err)
	}

	return c
}

func dbConn() (db *sql.DB) {
	var dbConfig DbConnect
	dbConfig.getConfigDB()
	db, err := sql.Open(dbConfig.Database.Type, dbConfig.Database.User+":"+dbConfig.Database.Password+"@/"+dbConfig.Database.Name)
	if err != nil {
		log.Error("Open connection failed:", err.Error())
		panic(err.Error())
	}
	return db
}

// insert data
func Process_new(table_name string, osArg []string) (err error){
	log.Debugf("Starting the application...")
	// read data
	csvFile, err := os.Open(osArg[1])
	if err != nil {
		log.Error("Can't open file:", osArg[1])
		log.Fatal(err)
	}
	defer csvFile.Close()
	csvLines, errFile := csv.NewReader(csvFile).ReadAll()
	if errFile != nil {
		log.Error("Can't read file:", osArg[1])
		log.Fatal(err)
	}

	// connect db
	db := dbConn()
	defer db.Close()

	// Create a new context, and begin a transaction
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("Begin transaction failed:", err.Error())
		log.Fatal(err)
	}

	// create table
	sqlCreateTable := `CREATE TABLE IF NOT EXISTS ` + table_name + `(`
	for key, column := range csvLines[0] {
		dataType := osArg[2+key]
		if s.Contains(dataType, "*") {
			dataType = s.ReplaceAll(dataType, `*`, `(`)
			dataType += `)`
		}
		sqlCreateTable += column + ` ` + dataType + `,`
	}
	sqlCreateTable = s.TrimSuffix(sqlCreateTable, `,`)
	sqlCreateTable += `);`

	// log
	log.Info(sqlCreateTable)
	_, err = tx.ExecContext(ctx, sqlCreateTable)
	if err != nil {
		log.Error("Create table failed:", err.Error())
		tx.Rollback()
		return
	}

	// save db
	sqlInsert := `INSERT INTO ` + table_name + ` VALUES `
	for i := 1; i < len(csvLines); i++ {
		sqlInsert += `(`
		for _, value := range csvLines[i] {
			if value == "" {
				sqlInsert += `NULL,`
			} else {
				sqlInsert += `'` + value + `'` + `,`
			}
		}
		sqlInsert = s.TrimSuffix(sqlInsert, `,`)
		sqlInsert += `),`
	}
	sqlInsert = s.TrimSuffix(sqlInsert, `,`)

	// log
	log.Info(sqlInsert)
	_, err = tx.ExecContext(ctx, sqlInsert)
	if err != nil {
		log.Error("Insert data failed:", err.Error())
		tx.Rollback()
		return
	}

	// commit transaction
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	return
}

// edit data
func Process_edit(table_name string, pathFile string) {
	// read data
	csvFile, err := os.Open(pathFile)
	if err != nil {
		log.Error("Can't open file:", pathFile)
		log.Fatal(err)
	}
	defer csvFile.Close()
	csvLines, errFile := csv.NewReader(csvFile).ReadAll()
	if errFile != nil {
		log.Error("Can't read file:", pathFile)
		log.Fatal(err)
	}

	// connect db
	db := dbConn()
	defer db.Close()

	// Create a new context, and begin a transaction
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("Begin transaction failed:", err.Error())
		log.Fatal(err)
	}

	// check if not exit id on file csv
	if csvLines[0][0] != "id" {
		log.Error("Not found id to edit")
		log.Fatal(err)
	}

	// update row
	for i := 1; i < len(csvLines); i++ {
		sqlEdit := ""
		sqlEdit += `UPDATE ` + table_name + ` SET `
		for j := 1; j < len(csvLines[i]); j++ {
			if csvLines[i][j] == "" {
				sqlEdit += csvLines[0][j] + `= NULL,`
			} else {
				sqlEdit += csvLines[0][j] + `='` + csvLines[i][j] + `',`
			}
		}
		sqlEdit = s.TrimSuffix(sqlEdit, `,`)
		sqlEdit += ` where id=` + csvLines[i][0] + `;`

		log.Info(sqlEdit)
		_, err = tx.Exec(sqlEdit)
		if err != nil {
			log.Error("Insert data failed:", err.Error())
			tx.Rollback()
			return
		}
	}

	// commit transaction
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
}

// delete data
func Process_del(table_name string, pathFile string) {
	// read data
	csvFile, err := os.Open(pathFile)
	if err != nil {
		log.Error("Can't open file:", pathFile)
		log.Fatal(err)
	}
	defer csvFile.Close()
	csvLines, errFile := csv.NewReader(csvFile).ReadAll()
	if errFile != nil {
		log.Error("Can't read file:", pathFile)
		log.Fatal(err)
	}

	// connect db
	db := dbConn()
	defer db.Close()

	// Create a new context, and begin a transaction
	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("Begin transaction failed:", err.Error())
		log.Fatal(err)
	}

	// check if not exit id on file csv
	if csvLines[0][0] != "id" || len(csvLines) == 1 {
		log.Error("Not found id to delete")
		log.Fatal(err)
	}

	// delete row
	sqlDelete := `DELETE FROM ` + table_name + ` WHERE id IN (`
	for i := 1; i < len(csvLines); i++ {
		_, err := strconv.Atoi(csvLines[i][0])
		if err != nil {
			log.Error("ID delete not format")
			log.Fatal(err)
		}
		sqlDelete += csvLines[i][0] + `,`
	}
	sqlDelete = s.TrimSuffix(sqlDelete, `,`)
	sqlDelete += `)`

	log.Info(sqlDelete)
	_, err = tx.Exec(sqlDelete)
	if err != nil {
		log.Error("Insert data failed:", err.Error())
		tx.Rollback()
		return
	}

	// commit transaction
	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
}

// export data
func Process_exp(table_name string, pathFile string) {
	// read data
	csvFile, err := os.Open(pathFile)
	if err != nil {
		log.Error("Can't open file:", pathFile)
		log.Fatal(err)
	}
	defer csvFile.Close()
	csvLines, errFile := csv.NewReader(csvFile).ReadAll()
	if errFile != nil {
		log.Error("Can't read file:", pathFile)
		log.Fatal(err)
	}

	// connect db
	db := dbConn()
	defer db.Close()

	// sql
	sqlGet := `SELECT `
	for _, column := range csvLines[0] {
		sqlGet += column + `,`
	}
	sqlGet = s.TrimSuffix(sqlGet, `,`)
	sqlGet += ` FROM ` + table_name
	log.Info(sqlGet)

	// get data
	rows, err := db.Query(sqlGet)
	if err != nil {
		log.Error("Get data failed:", err.Error())
		log.Fatal(err)
	}

	columns, err := rows.Columns()
	if err != nil {
		log.Error("Not get column failed:", err.Error())
		log.Fatal(err)
	}

	values := make([]sql.RawBytes, len(columns))

	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	//Save contents of all lines totalValues
	totalValues := make([][]string, 0)
	for rows.Next() {

		//Save the contents of each line
		var s []string

		//Add the contents of each line to scanArgs, and also to values
		err = rows.Scan(scanArgs...)
		if err != nil {
			log.Error(err.Error())
			panic(err.Error())
		}

		for _, v := range values {
			s = append(s, string(v))
		}
		totalValues = append(totalValues, s)
	}

	if err = rows.Err(); err != nil {
		panic(err.Error())
	}
	currentTime := time.Now()
	time := currentTime.Format("20060102-150405.000")
	time = s.ReplaceAll(time, ".", "-")

	writeToCSV(table_name+"_exp_"+time+".csv", columns, totalValues)
}

//writeToCSV
func writeToCSV(file string, columns []string, totalValues [][]string) {
	f, err := os.Create(file)
	// fmt.Println(columns)
	defer f.Close()
	if err != nil {
		panic(err)
	}
	//f.WriteString("\xEF\xBB\xBF")
	w := csv.NewWriter(f)
	for i, row := range totalValues {
		//First write column name + first row of data
		if i == 0 {
			w.Write(columns)
			w.Write(row)
		} else {
			w.Write(row)
		}
	}
	w.Flush()
	fmt.Println("Finished processing:", file)
}
